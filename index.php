<?php

session_start();

require_once 'vendor/autoload.php';
require_once 'vendor/slim/extras/Slim/Extras/Views/Twig.php';

$app = new \Slim\Slim();

$app->config('templates.path', 'templates');
$app->config('debug', 'true');

\Slim\Extras\Views\Twig::$twigExtensions = array(
    'Twig_Extensions_Slim',
);

$app->view(new \Slim\Extras\Views\Twig());

// this adds Slims's urlFor function to Twig templating
$env = $app->view()->getEnvironment();
$urlFor = new Twig_SimpleFunction('urlFor', function ($url, $data=null) use($app) {
    if(is_array($data)){
        return $app->urlFor($url,$data); // this makes sure the whole link is generated ie http://localhost....
    } else {
        return $app->urlFor($url);
    }
});
$env->addFunction($urlFor);


function helloworld(){
	$app = \Slim\Slim::getInstance();
	$app->render('helloworld.twig.html');
}

function welcome(){
    $app = \Slim\Slim::getInstance();
    $app->render('welcome.twig.html');
}

function downloads(){
    $app = \Slim\Slim::getInstance();
    $app->render('downloads.twig.html');
}

function profiles(){
    $app = \Slim\Slim::getInstance();
    $app->render('student_profiles.twig.html');
}

function profile($name){
    $app = \Slim\Slim::getInstance();
    $student -> name = $name;
    $app->view()->appendData(array("student"=>$student));
    $app->render('student_profile.twig.html');
}

function student_signup(){
    $app = \Slim\Slim::getInstance();
    $app->render('student_signup.twig.html');
}


$app->get('/signup/student', 'student_signup')->name('signup_student');
$app->get('/profile/:name', 'profile')->name('profile_student');
$app->get('/profiles', 'profiles')->name('profiles');
$app->get('/downloads', 'downloads')->name('downloads');
$app->get('/hello','helloworld')->name('hello');
$app->get('/', 'welcome')->name('home');


$app->run();

?>
